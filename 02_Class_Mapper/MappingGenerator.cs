﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace _02_Class_Mapper
{
    public class MappingGenerator
    {
        public Mapper<TSource, TDestination> Generate<TSource, TDestination>()
        {
            var sourceParam = Expression.Parameter(typeof(TSource));

            PropertyInfo[] sourceProperties = typeof(TSource).GetProperties();
            PropertyInfo[] destinationProperties = typeof(TDestination).GetProperties();

            var bindings = new List<MemberBinding>();
            foreach (var destinationProperty in destinationProperties)
            {
                var sourceProperty = sourceProperties.FirstOrDefault(propertyInfo => propertyInfo.Name == destinationProperty.Name);
                if (sourceProperty != null)
                {
                    bindings.Add(Expression.Bind(destinationProperty, Expression.Property(sourceParam, destinationProperty.Name)));
                }
            }

            Expression initializer = Expression.MemberInit(Expression.New(typeof(TDestination)), bindings);

            var mapFunction = Expression.Lambda<Func<TSource, TDestination>>(initializer, sourceParam);

            return new Mapper<TSource, TDestination>(mapFunction.Compile());
        }
    }
}
