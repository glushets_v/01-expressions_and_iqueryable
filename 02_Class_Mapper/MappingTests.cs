﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using _02_Class_Mapper.Models;

namespace _02_Class_Mapper
{
    [TestClass]
    public class MappingTests
    {
        [TestMethod]
        public void Copy_Foo_To_Bar_Success()
        {
            var mapGenerator = new MappingGenerator();
            var mapper = mapGenerator.Generate<Foo, Bar>();
            var source = new Foo { Name= "name", Email = "email@gmail.com" };

            var res = mapper.Map(source);

            Assert.AreEqual(source.Name, res.Name);
            Assert.AreEqual(source.Email, res.Email);
        }
    }
}
