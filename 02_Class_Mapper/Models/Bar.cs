﻿namespace _02_Class_Mapper.Models
{
    public class Bar
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}
