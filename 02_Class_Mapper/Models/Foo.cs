﻿namespace _02_Class_Mapper.Models
{
    public class Foo
    {
        public string Name { get; set; }

        public string Email { get; set; }
    }
}
