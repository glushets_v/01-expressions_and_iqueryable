﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace _01_Expression_Transformation
{
    class Program
    {
        static void Main(string[] args)
        {
            Expression<Func<int, int>> sourceExpAdd = x => x + 1;
            Expression<Func<int, int>> sourceExpSubtract = x => x - 1;

            var expressionTransformator = new BinaryTransformator();

            Expression<Func<int, int>> resultExpAdd = expressionTransformator.VisitAndConvert(sourceExpAdd, "");
            Expression<Func<int, int>> resultExpSubtract = expressionTransformator.VisitAndConvert(sourceExpSubtract, "");

            Console.WriteLine("Source: " + sourceExpAdd + " result: " + sourceExpAdd.Compile().Invoke(1));
            Console.WriteLine("Source: " + resultExpAdd + " result: " + resultExpAdd.Compile().Invoke(1));
            Console.WriteLine();
            Console.WriteLine("Source: " + sourceExpSubtract + " result: " + sourceExpSubtract.Compile().Invoke(1));
            Console.WriteLine("Source: " + resultExpSubtract + " result: " + resultExpSubtract.Compile().Invoke(1));

            Expression<Func<int, int, int, int>> sourceExpChange = (a, b, c) => a + b + c;
            var dic = new Dictionary<string, int>
            {
                {"c", 15}
            };

            Expression result = new ChangeParameterTransformator(dic).Visit(sourceExpChange);

            Expression<Func<int, int, int>> expr = result as Expression<Func<int, int, int>>;

            Console.WriteLine();
            Console.WriteLine(sourceExpChange + " result: " + sourceExpChange.Compile().Invoke(3, 2, 5));
            Console.WriteLine(result + " result: " + expr.Compile().Invoke(3, 2));
            Console.ReadLine();
        }
    }
}
