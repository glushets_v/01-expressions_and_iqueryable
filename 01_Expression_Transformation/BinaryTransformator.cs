﻿using System.Linq.Expressions;

namespace _01_Expression_Transformation
{
    public class BinaryTransformator : ExpressionVisitor
    {
        protected override Expression VisitBinary(BinaryExpression node)
        {
            ParameterExpression param = null;
            ConstantExpression constant = null;

            if (node.NodeType == ExpressionType.Add)
            {
                if (node.Left.NodeType == ExpressionType.Parameter && node.Left.Type == typeof(int))
                {
                    param = (ParameterExpression)node.Left;
                }

                if (node.Right.NodeType == ExpressionType.Constant)
                {
                    constant = (ConstantExpression)node.Right;
                }

                if (param != null && constant != null && (int)constant.Value == 1)
                {
                    return Expression.Increment(node.Left);
                }
            }
            else if (node.NodeType == ExpressionType.Subtract)
            {
                if (node.Left.NodeType == ExpressionType.Parameter && node.Left.Type == typeof(int))
                {
                    param = (ParameterExpression)node.Left;
                }

                if (node.Right.NodeType == ExpressionType.Constant)
                {
                    constant = (ConstantExpression)node.Right;
                }

                if (param != null && constant != null && (int)constant.Value == 1)
                {
                    return Expression.Decrement(node.Right);
                }
            }

            return base.VisitBinary(node);
        }
    }
}
