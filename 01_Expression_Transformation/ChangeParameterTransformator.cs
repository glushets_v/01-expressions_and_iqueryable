﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace _01_Expression_Transformation
{
    public class ChangeParameterTransformator : ExpressionVisitor
    {
        private readonly Dictionary<string, int> _paramsForChange;

        public ChangeParameterTransformator(Dictionary<string, int> paramsForChange)
        {
            this._paramsForChange = paramsForChange;
        }

        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            var parameters = node.Parameters.Where(x => GetValueForParameter(x.Name) == null).ToArray();
            return Expression.Lambda(Visit(node.Body), parameters);
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            var value = GetValueForParameter(node.Name);

            if (value != null)
            {
                return Expression.Constant(value);
            }

            return base.VisitParameter(node);
        }

        private int? GetValueForParameter(string key)
        {
            int value;
            if (_paramsForChange.TryGetValue(key, out value))
            {
                return value;
            }
            return null;
        }
    }
}
